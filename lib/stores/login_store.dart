import 'dart:collection';

import 'package:mobx/mobx.dart';
import 'package:wow_manager/core/api/login_api_service.dart';
import 'package:wow_manager/core/single_state.dart';
import 'package:wow_manager/screen/home/zone_data_repo.dart';

part 'login_store.g.dart';

class LoginStore = _LoginStore with _$LoginStore;

abstract class _LoginStore with Store {


  @action
  Future<bool> login(String imei) async {
    try {
      Map<String, String> user = HashMap();
      user["imei"] = imei;
      return await LoginApiService.login(user);
    } catch (e) {
      print(e);
    }
    return false;
  }
}
/// Build (generate .g.dart) flutter packages pub run build_runner build
/// Watch (update .g.dart automatically) flutter packages pub run build_runner watch
/// Clean before updating: flutter packages pub run build_runner watch --delete-conflicting-outputs
///