class AppDimens {
  static const spaceSmall = 4.0;
  static const spaceHalf = 8.0;
  static const spaceMedium = 16.0;
  static const spaceLarge = 32.0;
}
