import 'package:flutter/material.dart';

//Done use this. To get current theme: use Theme.of(context)
class AppTheme {
  static final Color _mainColor = Colors.blue;
  static final Color _iconColor = Colors.grey[700];

  static final ThemeData wowTheme = ThemeData(
      primarySwatch: _mainColor,
      appBarTheme: AppBarTheme(
          color: Colors.white,
          brightness: Brightness.dark,
          iconTheme: IconThemeData(color: _iconColor),
          actionsIconTheme: IconThemeData(color: _iconColor)),
      buttonTheme: ButtonThemeData(
        colorScheme: ColorScheme.dark(),
        buttonColor: _mainColor,
      ));
}
