import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:wow_manager/const/app_dimens.dart';
import 'package:wow_manager/langs/gen/strings.dart';
import 'package:wow_manager/notifications/push_messaging_example.dart';
import 'package:wow_manager/stores/login_store.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginState();
}

class _LoginState extends State<LoginPage> {
  LoginStore _loginStore;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _loginStore = LoginStore();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/bg_cyan.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.all(AppDimens.spaceLarge),
                child: SizedBox(
                  width: 140,
                  height: 140,
                  child: Container(
                    color: Theme.of(context).accentColor,
                  ),
                ),
              ),
              Container(
                width: 360,
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.all(AppDimens.spaceHalf),
                      child: TextField(
                        keyboardType: TextInputType.number,
                        inputFormatters: <TextInputFormatter>[
                          WhitelistingTextInputFormatter.digitsOnly
                        ],
                        obscureText: false,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: Strings.of(context).username,
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: AppDimens.spaceHalf,
                          left: AppDimens.spaceHalf,
                          right: AppDimens.spaceHalf),
                      child: TextField(
                        obscureText: true,
                        decoration: InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: Strings.of(context).passwords,
                        ),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerRight,
                      child: FlatButton(
                          onPressed: () {},
                          child: Text(
                            Strings.of(context).forgot_passwords,
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.bold),
                          )),
                    ),
                    Container(
                      width: double.infinity,
                      child: RaisedButton(
                        onPressed: () {
                          _onLoading();
//                          _loginStore.login("353415080716486");
//        Navigator.push(context, MaterialPageRoute(
//            builder: (_) => PushMessagingExample()
//        ));
                        },
                        child: Text(Strings.of(context).login.toUpperCase()),
                      ),
                    ),
                    SizedBox(
                      height: AppDimens.spaceMedium,
                    ),
                    Row(
                      children: <Widget>[
                        Expanded(
                          child: SizedBox(
                            height: 1,
                            child: Container(
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              left: AppDimens.spaceHalf,
                              right: AppDimens.spaceHalf),
                          child: Text(Strings.of(context).or_registry,
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor)),
                        ),
                        Expanded(
                          child: SizedBox(
                            height: 1,
                            child: Container(
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        )
                      ],
                    ),
                    MaterialButton(
                      minWidth: 360,
                      onPressed: () {},
                      child: Text(
                          Strings.of(context).registry_now.toUpperCase(),
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontWeight: FontWeight.bold)),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onLoading() async{
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
          child: Container(
            height: 120,
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                new CircularProgressIndicator(),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new Text("Loading"),
                ),
              ],
            ),
          ),
        );
      },
    );
    if (await _loginStore.login("353415080716486")) {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (_) => PushMessagingExample()));
    }
  }
}
