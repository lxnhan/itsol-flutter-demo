import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wow_manager/const/app_dimens.dart';

class SearchBar extends StatelessWidget {
  final title;
  final Function onTap;
  const SearchBar({@required this.title,@required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Card(
      borderOnForeground: false,
      elevation: 2,
      margin: EdgeInsets.all(AppDimens.spaceHalf),
      clipBehavior: Clip.hardEdge,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(4))
      ),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(AppDimens.spaceHalf),
          child: Row (
            children: <Widget>[
              Icon(Icons.search),
              SizedBox(width: AppDimens.spaceHalf),
              Text(title, style: TextStyle(color: Theme.of(context).hintColor))
            ],
          ),
        ),
      ),
    );
  }

}
