import 'package:flutter/material.dart';

class CustomTabBar extends StatefulWidget implements PreferredSizeWidget {
  final double _kTabHeight = 46.0;
  final double _kTextAndIconTabHeight = 72.0;
  final double indicatorWeight = 2.0;

  final List<Widget> tabs;
  final Function(int index) onPageChange;

  const CustomTabBar({@required this.tabs, @required this.onPageChange});

  @override
  State<StatefulWidget> createState() {
    return _CustomTabBarState();
  }

  @override
  Size get preferredSize {
    for (final Widget item in tabs) {
      if (item is Tab) {
        final Tab tab = item;
        if (tab.text != null && tab.icon != null)
          return Size.fromHeight(_kTextAndIconTabHeight + indicatorWeight);
      }
    }
    return Size.fromHeight(_kTabHeight + indicatorWeight);
  }
}

class _CustomTabBarState extends State<CustomTabBar> with SingleTickerProviderStateMixin {
  TabController tabController;

  @override
  void initState() {
    super.initState();
    tabController = TabController(
        length: widget.tabs.length,
        vsync: this,
        initialIndex: 0
    );
  }

  @override
  Widget build(BuildContext context) {
    return TabBar(
      onTap: widget.onPageChange,
      controller: tabController,
      labelColor: Theme.of(context).textTheme.bodyText1.color,
      unselectedLabelColor: Theme.of(context).hintColor,
      isScrollable: true,
      tabs: widget.tabs,
    );
  }

}