

import 'package:wow_manager/core/object/users.dart';

class ZoneDataRepo {
  Users users;

  static final ZoneDataRepo _cache = ZoneDataRepo._create();

  factory ZoneDataRepo() {
    return _cache;
  }

  static ZoneDataRepo get instance => ZoneDataRepo();

  ZoneDataRepo._create();
}