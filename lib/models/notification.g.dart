// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'notification.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

NotificationObj _$NotificationObjFromJson(Map json) {
  return NotificationObj(
    notifyTitle: NotifyTitle.fromJson(json['notifyTitle'] as Map),
  );
}

Map<String, dynamic> _$NotificationObjToJson(NotificationObj instance) =>
    <String, dynamic>{
      'notifyTitle': instance.notifyTitle,
    };

NotifyTitle _$NotifyTitleFromJson(Map json) {
  return NotifyTitle(
    json['title'] as String,
    json['body'] as String,
  );
}

Map<String, dynamic> _$NotifyTitleToJson(NotifyTitle instance) =>
    <String, dynamic>{
      'title': instance.title,
      'body': instance.body,
    };
