import 'package:json_annotation/json_annotation.dart';
part 'notification.g.dart';

@JsonSerializable(nullable: false, anyMap: true)
class NotificationObj {
  NotifyTitle notifyTitle;


  NotificationObj({this.notifyTitle});


  factory NotificationObj.fromJson(dynamic json) {
    final _notifyTitle = NotifyTitle.fromJson(json["notification"]);
    return NotificationObj(notifyTitle: _notifyTitle);
  }
  Map<String, dynamic> toJson() => _$NotificationObjToJson(this);

}

@JsonSerializable(nullable: false, anyMap: true)
class NotifyTitle{
  String title;
  String body;

  NotifyTitle(this.title, this.body);

  factory NotifyTitle.fromJson(Map<dynamic, dynamic> json) => _$NotifyTitleFromJson(json);
}