import 'package:intl/intl.dart';

class TimeUtils{

  static String getTimeString(int value) {
    final int hour = value ~/ 60;
    final int minutes = value % 60;
    return '${hour.toString().padLeft(2, "0")}:${minutes.toString().padLeft(2, "0")}';
  }

  static int getTimeInt(String value) {
    if(value.isEmpty) return 0;
    List<String> splitTime=value.split(":");
    return (int.parse(splitTime[0])*60)+(int.parse(splitTime[1]));
  }

  static String convertDate(int date){
    DateTime dateTime = DateTime.parse(date.toString());
    DateFormat dateFormat = DateFormat("dd/MM/yyyy", 'vi');
    return dateFormat.format(dateTime);
  }
}