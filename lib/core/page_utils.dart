import 'package:flutter/material.dart';
import 'package:wow_manager/const/app_dimens.dart';
import 'package:wow_manager/langs/gen/strings.dart';

Widget buildLoadingPageView() {
  return Scaffold(
    body: Center(
      child: CircularProgressIndicator(),
    ),
  );
}

Widget buildErrorPageView(BuildContext context, {String msg, @required Function onPressedRetry}) {
  return Scaffold(
    body: Container(
      padding: EdgeInsets.all(AppDimens.spaceLarge),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
//          Text(msg == null ? Strings.of(context).retry : msg, maxLines: 5),
//          RaisedButton(
//            child: Text(Strings.of(context).retry),
//            onPressed: onPressedRetry,
//          )
        ],
      ),
    ),
  );
}