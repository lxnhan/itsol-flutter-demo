// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'users.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Users _$UsersFromJson(Map json) {
  return Users(
    name: json['name'] as String,
    phone_number: json['phone_number'] as String,
    avatar: json['avatar'] as String,
  );
}

Map<String, dynamic> _$UsersToJson(Users instance) => <String, dynamic>{
      'phone_number': instance.phone_number,
      'name': instance.name,
      'avatar': instance.avatar,
    };
