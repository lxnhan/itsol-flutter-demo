import 'package:json_annotation/json_annotation.dart';
part 'users.g.dart';

@JsonSerializable(nullable: false, anyMap: true)
class Users {
  String phone_number;
  String name;
  String avatar;

  Users(
      {this.name,
        this.phone_number,
        this.avatar,
      });

  factory Users.fromJson(Map<String, dynamic> json) => _$UsersFromJson(json);
  Map<String, dynamic> toJson() => _$UsersToJson(this);
}