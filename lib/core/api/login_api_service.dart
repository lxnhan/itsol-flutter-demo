
import 'dart:convert';

import 'package:wow_manager/core/http/api_client.dart';
import 'package:wow_manager/core/http/wow_response_body.dart';
import 'package:wow_manager/core/object/users.dart';

class LoginApiService {
  static const PATH = '/login';

  static Future<bool> login(Map<String, String> user) async {
    WowResponseBody body = await ApiClient.instance.requestData(
        path: PATH,
        method: ApiClient.POST,
        data:jsonEncode(user),
    );
//    if(body.result){
//      return body.data["data"];
//    }
    return body.result;
  }

}