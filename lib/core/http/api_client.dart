import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/widgets.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:wow_manager/main.dart';

import 'wow_response_body.dart';

class ApiClient {
  static const String GET = 'GET';
  static const String POST = 'POST';
  static const String DELETE = 'DELETE';
  static const String PATCH = 'PATCH';
  static const String PUT = 'PUT';

  static final BaseOptions defaultOptions = BaseOptions(
    baseUrl: "http://103.109.33.65:30505/api",
    connectTimeout: 10000,
    receiveTimeout: 5000,
    contentType: 'application/json;charset=utf-8',
    headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJQMTYwMDA4NCIsInJvbGVzIjpbIlBBUlRORVIiXSwiaXNzIjoiRk1NLk1VUiIsImlhdCI6MTU4NDA2NDc5MiwiZXhwIjo0NzM3NjY0NzkyfQ.J-DG2huxCjo3bKyMYqST6NszrYyxDI2YkxjmRlQEeI3u1QwGRAIDByE4Va2QvyH5HAYl1_4M5bZeWZ8mtcnxUA'
    }
  );

  Dio _dio;

  static final Map<BaseOptions, ApiClient> _instanceMap = {};

  factory ApiClient({BaseOptions options}) {
    if(options == null)
      options = defaultOptions;
    if(_instanceMap.containsKey(options)) {
      return _instanceMap[options];
    }
    final ApiClient apiClient = ApiClient._create(options: options);
    _instanceMap[options] = apiClient;
    return apiClient;
  }

  ApiClient._create({BaseOptions options}) {
    if(options == null)
      options = defaultOptions;
    _dio = Dio(options);
    if(isDebugMode()) {
      _dio.interceptors.add(PrettyDioLogger(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
          responseHeader: false,
          error: true,
          compact: true,
          maxWidth: 180));
    }
  }

  static ApiClient get instance => ApiClient();

  Future<WowResponseBody> requestData({@required String path, @required String method, String data}) async {
    var response = await _dio.request(
        path,
        data: data,
        options: Options(method: method)
    );
    if (_isSuccessful(response.statusCode))
      return WowResponseBody.fromJson(response.data);
    throw('Request NOT successful :[code=${response.statusCode}, statusMessage=${response.statusMessage}');
  }

  Future<WowResponseBody> deleteData({@required String path, @required String method, String data}) async {
    var response = await _dio.delete(
        path,
        data: data,
        options: Options(method: method)
    );
    if (_isSuccessful(response.statusCode))
      return WowResponseBody.fromJson(response.data);
    throw('Request NOT successful :[code=${response.statusCode}, statusMessage=${response.statusMessage}');
  }

  bool _isSuccessful(int i) {
    return i >= 200 && i <= 299;
  }
}