import 'package:json_annotation/json_annotation.dart';

import 'wow_result.dart';
part 'wow_response_body.g.dart';

@JsonSerializable(nullable: false)
class WowResponseBody {
  bool result;
  int code;
  String message;
  Map data;

  WowResponseBody({this.result, this.data});

  factory WowResponseBody.fromJson(Map<String, dynamic> json) => _$WowResponseBodyFromJson(json);
  Map<String, dynamic> toJson() => _$WowResponseBodyToJson(this);

}