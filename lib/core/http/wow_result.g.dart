// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wow_result.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WowResult _$WowResultFromJson(Map<String, dynamic> json) {
  return WowResult(
    json['code'] as String,
    json['message'] as String,
    json['success'] as bool,
  );
}

Map<String, dynamic> _$WowResultToJson(WowResult instance) => <String, dynamic>{
      'code': instance.code,
      'message': instance.message,
      'success': instance.success,
    };
