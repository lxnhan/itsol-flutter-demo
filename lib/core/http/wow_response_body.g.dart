// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'wow_response_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

WowResponseBody _$WowResponseBodyFromJson(Map<String, dynamic> json) {
  return WowResponseBody(
    result: json['result'] as bool,
    data: json['data'] as Map<String, dynamic>,
  )
    ..code = json['code'] as int
    ..message = json['message'] as String;
}

Map<String, dynamic> _$WowResponseBodyToJson(WowResponseBody instance) =>
    <String, dynamic>{
      'result': instance.result,
      'code': instance.code,
      'message': instance.message,
      'data': instance.data,
    };
