import 'package:json_annotation/json_annotation.dart';
part 'wow_result.g.dart';

@JsonSerializable(nullable: false)
class WowResult {
  String code;
  String message;
  bool success;

  WowResult(this.code, this.message, this.success);

  factory WowResult.fromJson(Map<String, dynamic> json) => _$WowResultFromJson(json);
  Map<String, dynamic> toJson() => _$WowResultToJson(this);

}