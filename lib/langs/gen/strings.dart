import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:wow_manager/langs/vi.dart';


// ignore_for_file: non_constant_identifier_names
class Strings implements WidgetsLocalizations {

  String get isDefault => 'true';
  String get lang => 'vi';
  String get app_name => 'Flutter Base';
  String get username => 'imei';
  String get passwords => 'Mật khẩu';
  String get forgot_passwords => 'Quên mật khẩu';
  String get login => 'Đăng nhập';
  String get or_registry => 'Hoặc';
  String get registry_now => 'Đăng ký ngay';


  const Strings();

  static Strings current;

  static const GeneratedLocalizationsDelegate delegate = GeneratedLocalizationsDelegate();

  static Strings of(BuildContext context) => Localizations.of<Strings>(context, Strings);

  @override
  TextDirection get textDirection => TextDirection.ltr;
  
  static Locale localeResolutionCallback(Locale locale, Iterable<Locale> supported) {
    Locale target = _findSupported(locale);
    return target != null ? target : Locale("vi", "");
  }

  static Locale _findSupported(Locale locale) {
    if (locale != null) {
      for (Locale supportedLocale in delegate.supportedLocales) {
        if (locale.languageCode == supportedLocale.languageCode)
          return supportedLocale;
      }
    }
    return null;
  }
}

class GeneratedLocalizationsDelegate extends LocalizationsDelegate<Strings> {
  const GeneratedLocalizationsDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale("vi", ""),

    ];
  }

  LocaleListResolutionCallback listResolution({Locale fallback, bool withCountry = true}) {
    return (List<Locale> locales, Iterable<Locale> supported) {
      if (locales == null || locales.isEmpty) {
        return fallback ?? supported.first;
      } else {
        return _resolve(locales.first, fallback, supported, withCountry);
      }
    };
  }

  LocaleResolutionCallback resolution({Locale fallback, bool withCountry = true}) {
    return (Locale locale, Iterable<Locale> supported) {
      return _resolve(locale, fallback, supported, withCountry);
    };
  }

  @override
  Future<Strings> load(Locale locale) {
    final String lang = getLang(locale);
    if (lang != null) {
      switch (lang) {
        case "vi":
          Strings.current = const Vi();
          return SynchronousFuture<Strings>(Strings.current);

      }
    }
    Strings.current = const Strings();
    return SynchronousFuture<Strings>(Strings.current);
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale, true);

  @override
  bool shouldReload(GeneratedLocalizationsDelegate old) => false;

  ///
  /// Internal method to resolve a locale from a list of locales.
  ///
  Locale _resolve(Locale locale, Locale fallback, Iterable<Locale> supported, bool withCountry) {
    if (locale == null || !_isSupported(locale, withCountry)) {
      return fallback ?? supported.first;
    }

    final Locale languageLocale = Locale(locale.languageCode, "");
    if (supported.contains(locale)) {
      return locale;
    } else if (supported.contains(languageLocale)) {
      return languageLocale;
    } else {
      final Locale fallbackLocale = fallback ?? supported.first;
      return fallbackLocale;
    }
  }

  ///
  /// Returns true if the specified locale is supported, false otherwise.
  ///
  bool _isSupported(Locale locale, bool withCountry) {
    if (locale != null) {
      for (Locale supportedLocale in supportedLocales) {
        // Language must always match both locales.
        if (supportedLocale.languageCode != locale.languageCode) {
          continue;
        }

        // If country code matches, return this locale.
        if (supportedLocale.countryCode == locale.countryCode) {
          return true;
        }

        // If no country requirement is requested, check if this locale has no country.
        if (true != withCountry && (supportedLocale.countryCode == null || supportedLocale.countryCode.isEmpty)) {
          return true;
        }
      }
    }
    return false;
  }
}

String getLang(Locale l) => l == null
    ? null
    : l.countryCode != null && l.countryCode.isEmpty
    ? l.languageCode
    : l.toString();
