import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:wow_manager/const/app_theme.dart';
import 'package:wow_manager/langs/gen/strings.dart';
import 'package:wow_manager/screen/login/loginPage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
        Strings.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: Strings.delegate.supportedLocales,
      localeResolutionCallback: Strings.localeResolutionCallback,
      title: 'WowSport Manager',
      theme: AppTheme.wowTheme,
      home: LoginPage(),
    );
  }
}

bool isDebugMode() {
  bool inDebugMode = false;
  assert(inDebugMode = true);
  return inDebugMode;
}
