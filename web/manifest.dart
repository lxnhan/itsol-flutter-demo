import 'package:wow_manager/langs/gen/strings.dart';

// ignore_for_file: non_constant_identifier_names
class Manifest extends Strings {
  const Manifest();
  @override
  String get name => 'wow_manager';
  @override
  String get short_name => 'wow_manager';
  @override
  String get start_url => '.';
  @override
  String get display => 'minimal-ui';
  @override
  String get background_color => '#0175C2';
  @override
  String get theme_color => '#0175C2';
  @override
  String get description => 'WowSport Manager';
  @override
  String get orientation => 'portrait-primary';
  @override
  String get prefer_related_applications => 'false';
  @override
  String get icons => '[{src: icons/Icon-192.png, sizes: 192x192, type: image/png}, {src: icons/Icon-512.png, sizes: 512x512, type: image/png}]';

}